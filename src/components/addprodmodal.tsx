import React from 'react';
import tw from 'twin.macro';
import Link from 'next/link';
import { useRouter } from 'next/router';
import axios from '@/app/utils/api';
import { useAppContext } from '@/app/context/AppContext';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useAlert } from 'react-alert';
import { IoIosAddCircleOutline } from 'react-icons/io';
export default function AddProductModal() {
  const [showModal, setShowModal] = React.useState(false);

  const alert = useAlert();
  const { register, handleSubmit } = useForm();

  const router = useRouter();

  const onSubmit = (values) => {
    const obj = JSON.parse(JSON.stringify(values));
    console.log(obj);
    axios
      .post('/product/add_product', {
        barcode: obj.barcode,
        prod_name: obj.pname,
        prod_desc: obj.pdesc,
        prod_price: obj.pprice,
        prod_stocks: obj.pstocks,
      })
      .then((res) => {
        alert.success(res.data.data);
        setShowModal(false);
      });
  };

  return (
    <>
      <button
        tw="ml-4  rounded px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75"
        type="button"
        onClick={() => setShowModal(true)}
      >
        <IoIosAddCircleOutline tw="h-8 w-8" />
      </button>
      {showModal ? (
        <>
          <div tw="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div tw="relative w-auto my-6 mx-auto max-w-3xl">
              {/*content*/}
              <div tw="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div tw="bg-blueGray-500 flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                  <h3 tw="text-3xl text-white font-semibold">Product Information</h3>
                  <button
                    tw="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={() => setShowModal(false)}
                  >
                    <span tw="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">×</span>
                  </button>
                </div>
                {/*body*/}

                <form onSubmit={handleSubmit(onSubmit)}>
                  <div tw="p-3 ">
                    <div tw="mb-3 pt-0">
                      <input
                        {...register('barcode')}
                        type="number"
                        placeholder="Barcode"
                        tw="border px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow w-full"
                      />
                    </div>
                    <div tw="mb-3 pt-0">
                      <input
                        {...register('pname')}
                        type="text"
                        placeholder="Product Name"
                        tw="border px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow w-full"
                      />
                    </div>
                    <div tw="mb-3 pt-0">
                      <input
                        {...register('pdesc')}
                        type="text"
                        placeholder="Description"
                        tw="border px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow w-full"
                      />
                    </div>
                    <div tw="mb-3 pt-0">
                      <input
                        {...register('pprice')}
                        type="number"
                        placeholder="Price"
                        tw="border px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow w-full"
                      />
                    </div>
                    <div tw="mb-3 pt-0">
                      <input
                        {...register('pstocks')}
                        type="number"
                        placeholder="Stocks"
                        tw="border px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow w-full"
                      />
                    </div>
                  </div>

                  {/*footer*/}
                  <div tw="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
                    <button
                      tw="text-red-500 bg-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                      type="button"
                      onClick={() => setShowModal(false)}
                    >
                      Close
                    </button>
                    <button
                      tw="bg-blueGray-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                      type="submit"
                    >
                      Add product
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div tw="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
}
