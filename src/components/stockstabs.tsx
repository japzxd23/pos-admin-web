import React from 'react';
import tw from 'twin.macro';
import AddProductModal from './addprodmodal';

import MdProductionQuantityLimits from 'react-icons/md';
const Tabs = () => {
  return (
    <div tw="flex flex-wrap mt-5">
      <div tw="w-full px-4">
        <nav tw="relative flex flex-wrap items-center justify-between px-2 py-3  bg-blueGray-500 rounded">
          <div tw="container px-4 mx-auto flex flex-wrap items-center justify-between">
            <div tw="w-full relative flex justify-between lg:w-auto px-4 lg:static lg:block lg:justify-start">
              <a tw="text-sm font-bold leading-relaxed inline-block mr-4 py-2 uppercase text-white">Products</a>
            </div>
            <div tw="flex lg:flex-grow items-center" id="example-navbar-info">
              <ul tw="flex flex-col lg:flex-row list-none ml-auto">
                <li tw="">
                  <AddProductModal />
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
  );
};

export default Tabs;
