import React, { useEffect } from 'react';
import tw from 'twin.macro';
import Link from 'next/link';
import { useRouter } from 'next/router';
import axios from '@/app/utils/api';

import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useAlert } from 'react-alert';
import { AiFillEdit } from 'react-icons/ai';

export default function EditProductModal(props) {
  const [showModal, setShowModal] = useState(false);
  const [editProduct, setEditProduct] = useState([]);

  const edit = (data) => {
    const req = async () => {
      const prod = await axios.get('/product/view/' + data).then((res) => res.data.data);
      setEditProduct(prod);

      setShowModal(true);
    };
    req();
  };

  const alert = useAlert();
  const { register, handleSubmit } = useForm();

  const router = useRouter();

  const onSubmit = (values) => {
    const obj = JSON.parse(JSON.stringify(values));
    console.log(obj);
    axios
      .put('/product/update_product/' + props.bcode, {
        barcode: props.bcode,
        prod_name: obj.pname,
        prod_desc: obj.pdesc,
        prod_price: obj.pprice,
        prod_stocks: obj.pstocks,
      })
      .then((res) => {
        alert.success(res.data.data);
        setShowModal(false);
      });
  };

  return (
    <>
      <button type="button" onClick={() => edit(props.bcode)}>
        <AiFillEdit tw="h-5 w-5 text-sky-600" />
      </button>
      {showModal ? (
        <>
          <div tw="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
            <div tw="relative w-auto my-6 mx-auto max-w-3xl">
              {/*content*/}
              <div tw="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div tw="bg-blueGray-500 flex items-start justify-between p-5 border-b border-solid border-blueGray-200 rounded-t">
                  <h3 tw="text-3xl text-white font-semibold">Product Information</h3>
                  <button
                    tw="p-1 ml-auto bg-transparent border-0 text-black opacity-5 float-right text-3xl leading-none font-semibold outline-none focus:outline-none"
                    onClick={() => setShowModal(false)}
                  >
                    <span tw="bg-transparent text-black opacity-5 h-6 w-6 text-2xl block outline-none focus:outline-none">×</span>
                  </button>
                </div>
                {/*body*/}

                <form onSubmit={handleSubmit(onSubmit)}>
                  {editProduct.map((data, index) => (
                    <div key={index} tw="p-3 ">
                      <div tw="mb-3 pt-0">
                        <input
                          {...register('barcode')}
                          type="number"
                          value={data.barcode}
                          tw="border px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow w-full"
                        />
                      </div>
                      <div tw="mb-3 pt-0">
                        <input
                          {...register('pname')}
                          type="text"
                          placeholder={data.prod_name}
                          tw="border px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow w-full"
                        />
                      </div>
                      <div tw="mb-3 pt-0">
                        <input
                          {...register('pdesc')}
                          type="text"
                          placeholder={data.prod_desc}
                          tw="border px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow w-full"
                        />
                      </div>
                      <div tw="mb-3 pt-0">
                        <input
                          {...register('pprice')}
                          type="number"
                          placeholder={data.prod_price}
                          tw="border px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow w-full"
                        />
                      </div>
                      <div tw="mb-3 pt-0">
                        <input
                          {...register('pstocks')}
                          type="number"
                          placeholder={data.prod_stocks}
                          tw="border px-3 py-3 placeholder-blueGray-300 text-blueGray-600 relative bg-white bg-white rounded text-sm shadow outline-none focus:outline-none focus:shadow w-full"
                        />
                      </div>
                    </div>
                  ))}
                  {/*footer*/}
                  <div tw="flex items-center justify-end p-6 border-t border-solid border-blueGray-200 rounded-b">
                    <button
                      tw="text-red-500 bg-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                      type="button"
                      onClick={() => setShowModal(false)}
                    >
                      Close
                    </button>
                    <button
                      tw="bg-blueGray-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                      type="submit"
                    >
                      Update product
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div tw="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
}
