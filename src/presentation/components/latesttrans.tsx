import React from 'react';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { HiMenu } from 'react-icons/hi';
import Link from 'next/link';
import tw, { styled } from 'twin.macro';

export default function Lastesttrans() {
  const [latest, setLatest] = useState([]);

  const [onlinestaffz, setOnlineStaff] = useState([]);
  const [todaysales, setTodaySales] = useState([]);
  const [statsale, setStatSale] = useState([]);
  const [statvoid, setStatVoid] = useState([]);

  const router = useRouter();
  useEffect(() => {
    const req = async () => {
      const prod = await axios.get('../api/hello').then((res) => res.data);
      setLatest(prod.latest);

      setOnlineStaff(prod.onlinestaff);
      setTodaySales(prod.today_sales);
      setStatSale(prod.stat_sale);
      setStatVoid(prod.stat_void);
    };
    req();
  }, []);
  return (
    <div tw="flex flex-col">
      <div tw="overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div tw="inline-block max-w-full sm:px-6 lg:px-8">
          <div tw="overflow-hidden">
            <table tw="w-full">
              <thead tw="bg-gradient-to-b from-gray-200 to-gray-100 border-b">
                <tr>
                  <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                    Date
                  </th>
                  <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                    Invoice
                  </th>
                  <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                    Sales
                  </th>
                  <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                    Cashier
                  </th>
                </tr>
              </thead>
              <tbody>
                {latest.map((latest, index) => (
                  <tr tw="bg-white border-b transition duration-300 ease-in-out hover:bg-gray-100">
                    <td key={latest.date} tw="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                      {latest.date}
                    </td>
                    <td tw="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                      <button tw="text-sky-700">{latest.invoice}</button>
                    </td>
                    <td tw="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{latest.sales}</td>
                    <td tw="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{latest.cashier}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}
