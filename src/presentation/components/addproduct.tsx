import React from 'react';
import tw from 'twin.macro';
import Link from 'next/link';
import { useRouter } from 'next/router';
import axios from '@/app/utils/api';
import { useAppContext } from '@/app/context/AppContext';
import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useAlert } from 'react-alert';

const Addproduct = () => {
  const { setAddProductDialog } = useAppContext();
  const alert = useAlert();
  const { register, handleSubmit } = useForm();

  const router = useRouter();

  const onSubmit = (values) => {
    const obj = JSON.parse(JSON.stringify(values));
    console.log(obj);
    axios
      .post('/product/add_product', {
        barcode: obj.barcode,
        prod_name: obj.pname,
        prod_desc: obj.pdesc,
        prod_price: obj.pprice,
        prod_stocks: obj.pstocks,
      })
      .then((res) => {
        alert.success(res.data.data);
        setAddProductDialog(false);
      });
  };

  return (
    <div tw="flex flex-col">
      <div tw="container  flex-1 flex flex-col items-center  px-2">
        <div tw="bg-white px-6 py-8 rounded shadow-md border-2 border-sky-800 text-black w-full">
          <h1 tw="mb-8 text-3xl text-center">Add Product</h1>
          <form onSubmit={handleSubmit(onSubmit)}>
            <input {...register('barcode')} type="number" tw="block border  p-3 rounded mb-4" name="barcode" placeholder="Barcode" />
            <input {...register('pname')} type="text" tw="block border  p-3 rounded mb-4" name="pname" placeholder="Product Name" />

            <input {...register('pdesc')} type="text" tw="block border p-3 rounded mb-4" name="pdesc" placeholder="Description" />

            <input {...register('pprice')} type="number" tw="block border  p-3 rounded mb-4" name="pprice" placeholder="Price" />
            <input {...register('pstocks')} type="number" tw="block border  p-3 rounded mb-4" name="pstocks" placeholder="Stocks" />

            <button type="submit" tw="flex w-full text-center p-2 rounded bg-sky-800 text-white hover:bg-amber-400 focus:outline-none my-1">
              <p tw="flex w-full justify-center">Create</p>
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Addproduct;
