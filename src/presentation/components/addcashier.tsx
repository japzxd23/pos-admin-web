import React from 'react';
import tw from 'twin.macro';
import { useRouter } from 'next/router';
import axios from '@/app/utils/api';
import { useAppContext } from '@/app/context/AppContext';
import { MAIN_URL } from './masthead';
import { useForm } from 'react-hook-form';
import { useAlert } from 'react-alert';

const Addcashier = () => {
  const { setAddCashierDialog } = useAppContext();
  const URL = `${MAIN_URL}`;
  const { register, handleSubmit } = useForm();
  const alert = useAlert();
  const router = useRouter();

  const onSubmit = (values) => {
    const obj = JSON.parse(JSON.stringify(values));
    axios
      .post('/add_cashier', {
        fullname: obj.cashier,
        user_name: obj.uname,
        password: obj.password,
        email: obj.email,
      })
      .then((res) => {
        console.log(res);
        alert.success(res.data.data);
        setAddCashierDialog(false);
      });
  };

  return (
    <div tw="flex flex-col">
      <div tw="container  flex-1 flex flex-col items-center  px-2">
        <div tw="bg-white px-6 py-8 rounded shadow-md border-2 border-sky-800 text-black w-full">
          <h1 tw="mb-8 text-3xl text-center">Add Cashier</h1>
          <form onSubmit={handleSubmit(onSubmit)}>
            <input {...register('cashier')} type="text" tw="block border  p-3 rounded mb-4" name="cashier" placeholder="Cashier Name" />
            <input {...register('uname')} type="text" tw="block border  p-3 rounded mb-4" name="uname" placeholder="Username" />

            <input {...register('email')} type="text" tw="block border p-3 rounded mb-4" name="email" placeholder="Email" />

            <input {...register('password')} type="password" tw="block border  p-3 rounded mb-4" name="password" placeholder="Password" />

            <button type="submit" tw="flex w-full text-center p-2 rounded bg-sky-800 text-white hover:bg-amber-400 focus:outline-none my-1">
              <p tw="flex w-full justify-center">Create</p>
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Addcashier;
