import React from 'react';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { HiMenu } from 'react-icons/hi';
import Link from 'next/link';
import tw, { styled } from 'twin.macro';

export default function Lowstocks() {
  const [lowstocks, setLowStocks] = useState([]);

  const router = useRouter();
  useEffect(() => {
    const req = async () => {
      const prod = await axios.get('../api/hello').then((res) => res.data);

      setLowStocks(prod.lowstocks);
    };
    req();
  }, []);
  return (
    <div tw="flex flex-col">
      <div tw="overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div tw="inline-block max-w-full sm:px-6 lg:px-8">
          <div tw="overflow-hidden">
            <table tw="w-full">
              <thead tw="bg-gradient-to-b from-gray-200 to-gray-100 border-b">
                <tr>
                  <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                    ID
                  </th>
                  <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                    Barcode
                  </th>
                  <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                    Product
                  </th>
                  <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                    Stocks
                  </th>
                </tr>
              </thead>
              <tbody>
                {lowstocks.map((lowstocks, index) => (
                  <tr tw="bg-white border-b transition duration-300 ease-in-out hover:bg-gray-100">
                    <td key={lowstocks.id} tw="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                      {lowstocks.id}
                    </td>
                    <td tw="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                      <button tw="text-sky-700">{lowstocks.barcode}</button>
                    </td>
                    <td tw="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{lowstocks.product}</td>
                    <td tw="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{lowstocks.stocks}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

const ListBtn = tw.button`
text-left bg-white py-1 px-4 text-black
hover:(bg-sky-700) 
active:(scale-95)
`;

const NavBtn = tw.button`
text-white
hover:(text-amber-600 scale-95)
`;

const MobileNavItem = tw.button`
  outline-none text-white py-1 px-4
  hover:(text-amber-500)
  active:(scale-90)
`;
