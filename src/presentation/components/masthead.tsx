import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

import { HiMenu } from 'react-icons/hi';
import Link from 'next/link';
import tw, { styled } from 'twin.macro';

export const MAIN_URL = 'http://139.99.52.160:3333/';

export default function Masthead() {
  const router = useRouter();
  const [menu, setMenu] = useState(false);

  const logOut = () => {
    localStorage.removeItem('token');
    router.push('/');
  };

  const toggleMenu = () => setMenu(!menu);

  return (
    <nav tw="">
      <div tw="">
        <div tw="relative flex justify-between items-center bg-blueGray-700 py-4 w-screen px-4 z-[2]">
          <p tw="text-white">Dashboard</p>

          {/* Mobile */}
          <div tw="md:(hidden)">
            <button tw="border border-gray-400 rounded-md p-2 active:(scale-90) outline-none" onClick={toggleMenu}>
              <HiMenu tw="w-4 h-4 text-white" />
            </button>
          </div>

          {/* bigger screen */}
        </div>

        <div tw="relative">
          <div
            tw="absolute inset-x-0 z-[1] opacity-95 bg-blueGray-500 flex flex-col space-y-2 py-2 transition-all duration-1000 ease-in-out"
            css={menu ? tw`` : tw`mt-[-428px]`}
          >
            <MobileNavItem onClick={() => router.push('/dashboard/cashiers')}>Cashier</MobileNavItem>
            <MobileNavItem onClick={() => router.push('/dashboard/stocks')}>Stocks</MobileNavItem>
            <MobileNavItem>Reports</MobileNavItem>
            <MobileNavItem onClick={() => logOut()}>Logout</MobileNavItem>
          </div>
        </div>
      </div>
    </nav>
  );
}

const ListBtn = tw.button`
text-left bg-white py-1 px-4 text-black
hover:(bg-sky-700) 
active:(scale-95)
`;

const NavBtn = tw.button`
text-white
hover:(text-amber-600 scale-95)
`;

const MobileNavItem = tw.button`
  outline-none text-white py-1 px-4
  hover:(text-amber-500)
  active:(scale-90)
`;
