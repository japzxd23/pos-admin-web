import React, { useEffect } from 'react';
import { Menu, Transition } from '@headlessui/react';
import { MdExpandMore } from 'react-icons/md';
import tw, { styled } from 'twin.macro';

export default function Productdialog() {
  return (
    <div tw="flex justify-center">
      <div tw="relative">
        <Menu>
          <Menu.Button tw="px-3 py-1 border  hover:(bg-amber-100) border-gray-300">
            <div tw="flex items-center space-x-2">
              <span>Select</span>
              <MdExpandMore tw="h-4 w-4" />
            </div>
          </Menu.Button>

          <ContentTransition enter="enter" enterFrom="enterFrom" enterTo="enterTo" leave="leave" leaveFrom="leaveFrom" leaveTo="leaveTo">
            <Menu.Items tw="absolute left-[4rem] z-[1] py-2 flex flex-col bg-white rounded-lg shadow-md max-w-max">
              <Menu.Item>
                {({ active }) => (
                  <button tw="flex-shrink-0 w-[12rem] py-3 px-4 text-left transition duration-100 ease-in-out" css={[active && tw`bg-gray-200`]}>
                    Edit Product
                  </button>
                )}
              </Menu.Item>
              <Menu.Item>
                {({ active }) => (
                  <button tw="flex-shrink-0 w-[12rem] py-3 px-4 text-left transition duration-100 ease-in-out" css={active && tw`bg-gray-200`}>
                    Add Stocks
                  </button>
                )}
              </Menu.Item>
              <Menu.Item>
                {({ active }) => (
                  <button tw="flex-shrink-0 w-[12rem] py-3 px-4 text-left transition duration-100 ease-in-out" css={active && tw`bg-red-200`}>
                    Remove
                  </button>
                )}
              </Menu.Item>
            </Menu.Items>
          </ContentTransition>
        </Menu>
      </div>
    </div>
  );
}

const ContentTransition = styled(Transition)`
  &.enter {
    ${tw`ease-out duration-100`}
  }
  &.enterFrom {
    ${tw`opacity-0`}
  }
  &.enterTo {
    ${tw`opacity-100 z-[1]`}
  }
  &.leave {
    ${tw`ease-in duration-100`}
  }
  &.leaveFrom {
    ${tw`opacity-100`}
  }
  &.leaveTo {
    ${tw`opacity-0`}
  }
`;
