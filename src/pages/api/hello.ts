export default (req, res) => {
  res.status(200).json({
    latest: [
      { id: 1, date: '05/22/2022', invoice: 185, sales: 25, cashier: 'Japz' },
      { id: 2, date: '05/20/2022', invoice: 173, sales: 123, cashier: 'Japz' },
      { id: 3, date: '05/19/2022', invoice: 165, sales: 42, cashier: 'Japz' },
    ],

    lowstocks: [
      { id: 1, barcode: 5551, product: 'sunsilk', stocks: 4 },
      { id: 2, barcode: 5555, product: 'Emperador lights', stocks: 3 },
      { id: 3, barcode: 5551, product: 'Red Horse 500ML', stocks: 4 },
    ],

    onlinestaff: 'Japz',

    today_sales: 102544,

    stat_sale: 154,

    stat_void: 6,
  });
};
