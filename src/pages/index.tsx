import { useAlert } from 'react-alert';
import { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import tw from 'twin.macro';
import { useForm } from 'react-hook-form';
import { MAIN_URL } from '@/presentation/components/masthead';
import AddProductModal from 'src/components/addprodmodal';

export default function Home() {
  const [isDisabled, setIsDisabled] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const alert = useAlert();
  const router = useRouter();

  const onSubmit = (valuex) => {
    setIsDisabled(true);
    console.log(valuex);
    const obj = JSON.parse(JSON.stringify(valuex));
    axios
      .post(`${MAIN_URL}admin`, {
        user_name: obj.uname,
        password: obj.password,
      })
      .then((res) => {
        localStorage.setItem('token', res.data.token);
        router.push('/dashboard');
        {
          /* if (res.data == 'SUCCESS') {
          localStorage.setItem('logged', 'granted');

       
        } else {
          alert.error('Invalid login.');
          setIsDisabled(false);
        }*/
        }
      });
  };

  return (
    <div>
      <section tw="h-screen">
        <div tw="container px-6 py-12 h-full">
          <div tw="flex justify-center items-center flex-wrap h-full text-gray-800">
            <div tw="md:w-8/12 lg:w-6/12 mb-12 md:mb-0">
              <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.svg" tw="w-full" alt="Phone image" />
            </div>

            <div tw="md:w-8/12 lg:w-5/12 lg:ml-20 bg-white p-5 rounded shadow border">
              <form tw="bg-white bg-opacity-80 p-4 rounded-md" onSubmit={handleSubmit(onSubmit)}>
                <div tw="mb-6">
                  <input
                    {...register('uname', { required: 'Please enter username.' })}
                    type="text"
                    tw=" block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    placeholder="Username"
                  />
                </div>

                <div tw="mb-6">
                  <input
                    {...register('password', { required: 'Please enter password.' })}
                    type="password"
                    tw=" block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    placeholder="Password"
                  />
                </div>

                <div tw="flex justify-between items-center mb-6"></div>

                <button
                  disabled={isDisabled}
                  type="submit"
                  tw="inline-block px-7 py-3 bg-blue-600 text-white font-medium text-sm leading-snug uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out w-full"
                  data-mdb-ripple="true"
                  data-mdb-ripple-color="light"
                >
                  Sign in
                </button>
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

const Title = tw.span`
font-family['Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif]
font-bold
text-gray-800
font-size[30px]
`;

const styles = {
  // Move long tw sets out of jsx to keep it scannable
  container: ({ hasBackground }) => [tw`flex flex-col items-center justify-center h-screen`, hasBackground && tw`bg-gradient-to-b from-sky-500 to-gray-700`],
};
