import { useAlert } from 'react-alert';
import { useEffect, useState } from 'react';
import axios from 'axios';
import Head from 'next/head';
import Link from 'next/link';
import Masthead from '@/presentation/components/masthead';
import tw from 'twin.macro';
import { useRouter } from 'next/router';
import { FaCoins } from 'react-icons/fa';
import { BsFillCartCheckFill, BsCalendarDayFill } from 'react-icons/bs';
import { GiCancel } from 'react-icons/gi';

export default function Dashboard() {
  const alert = useAlert();
  const [latest, setLatest] = useState([]);
  const [lowstocks, setLowStocks] = useState([]);
  const [onlinestaffz, setOnlineStaff] = useState([]);
  const [todaysales, setTodaySales] = useState([]);
  const [statsale, setStatSale] = useState([]);
  const [statvoid, setStatVoid] = useState([]);

  const router = useRouter();
  useEffect(() => {
    const req = async () => {
      const prod = await axios.get('../api/hello').then((res) => res.data);
      setLatest(prod.latest);
      setLowStocks(prod.lowstocks);
      setOnlineStaff(prod.onlinestaff);
      setTodaySales(prod.today_sales);
      setStatSale(prod.stat_sale);
      setStatVoid(prod.stat_void);
    };
    req();
  }, []);
  const logOut = () => {
    localStorage.removeItem('token');
    router.push('/');
  };
  return (
    <div>
      <Head>
        <title>Point of Sales - Cashier</title>
        <meta name="description" content="POS" />
        <link rel="icon" href="/favicon.ico" />

        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet"
        />
      </Head>
      <Masthead />

      <div tw=" flex bg-white  ">
        <div tw="md:ml-40 p-2">
          Home
          <div tw="flex flex-col space-y-5">
            <div tw="flex items-center ">
              <BsCalendarDayFill tw="w-5 h-5" />
              <h6 tw=" p-1">05/26/2022</h6>
            </div>
            <div tw="  md:flex ">
              <div tw="flex flex-col shadow rounded bg-gradient-to-b from-sky-800 to-sky-400 py-2 px-3 md:flex-1">
                <h6 tw="text-white p-1">Latest Transaction</h6>
                <div tw="flex flex-col">
                  <div tw="overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div tw="inline-block max-w-full sm:px-6 lg:px-8">
                      <div tw="overflow-hidden">
                        <table tw="w-full">
                          <thead tw="bg-gradient-to-b from-gray-200 to-gray-100 border-b">
                            <tr>
                              <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                Date
                              </th>
                              <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                Invoice
                              </th>
                              <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                Sales
                              </th>
                              <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                Cashier
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {latest.map((latest, index) => (
                              <tr tw="bg-white border-b transition duration-300 ease-in-out hover:bg-gray-100">
                                <td key={latest.date} tw="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                  {latest.date}
                                </td>
                                <td tw="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                  <button tw="text-sky-700">{latest.invoice}</button>
                                </td>
                                <td tw="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{latest.sales}</td>
                                <td tw="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{latest.cashier}</td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div tw="mt-5 py-2 px-3 shadow rounded bg-gradient-to-b from-red-400 to-red-300 flex flex-col md:(flex-1 mt-0 ml-4) ">
                <h6 tw="text-white p-1">Low Stocks</h6>
                <div tw="flex flex-col">
                  <div tw="overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div tw="inline-block max-w-full sm:px-6 lg:px-8">
                      <div tw="overflow-hidden">
                        <table tw="w-full">
                          <thead tw="bg-gradient-to-b from-gray-200 to-gray-100 border-b">
                            <tr>
                              <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                ID
                              </th>
                              <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                Barcode
                              </th>
                              <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                Product
                              </th>
                              <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-4 text-left">
                                Stocks
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            {lowstocks.map((lowstocks, indexx) => (
                              <tr tw="bg-white border-b transition duration-300 ease-in-out hover:bg-gray-100">
                                <td key={lowstocks.id} tw="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                                  {lowstocks.id}
                                </td>
                                <td tw="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">
                                  <button tw="text-sky-700">{lowstocks.barcode}</button>
                                </td>
                                <td tw="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{lowstocks.product}</td>
                                <td tw="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">{lowstocks.stocks}</td>
                              </tr>
                            ))}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div tw="bg-white border shadow-md rounded p-2 space-y-5">
              <h4 tw="font-medium leading-tight text-2xl">Today's Income</h4>
              <div tw=" p-2 flex items-center justify-between border-b">
                <FaCoins tw="text-yellow-500 w-10 h-10" />
                <h4 tw="font-medium leading-tight text-2xl text-gray-500">15465</h4>
              </div>
              <h4 tw="font-medium leading-tight text-2xl">Statistics</h4>
              <div tw=" p-2 flex items-center justify-between ">
                <BsFillCartCheckFill tw="text-emerald-400 w-10 h-10" />
                <h4 tw="font-medium leading-tight text-2xl text-gray-500">125</h4>
              </div>
              <div tw=" p-2 flex items-center justify-between ">
                <GiCancel tw="text-red-500 w-10 h-10" />
                <h4 tw="font-medium leading-tight text-2xl text-gray-500">12</h4>
              </div>
            </div>
          </div>
        </div>

        {/* BIG SCREEN*/}

        <div tw=" hidden md:flex flex-col w-40 h-full shadow bg-gradient-to-b from-gray-700 to-black px-1 absolute">
          <ul key={555} tw="relative">
            <li tw="relative">
              <a
                tw="flex items-center text-sm py-4 px-6 h-12 overflow-hidden text-white overflow-ellipsis whitespace-nowrap rounded hover:text-gray-900 hover:bg-gray-100 transition duration-300 ease-in-out"
                href="#!"
                data-mdb-ripple="true"
                data-mdb-ripple-color="dark"
              >
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  tw="w-3 h-3 mr-3"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                >
                  <path
                    fill="currentColor"
                    d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"
                  ></path>
                </svg>
                <span>Cashier</span>
              </a>
            </li>
            <li tw="relative">
              <a
                tw="flex items-center text-sm py-4 px-6 h-12 overflow-hidden text-white overflow-ellipsis whitespace-nowrap rounded hover:text-gray-900 hover:bg-gray-100 transition duration-300 ease-in-out"
                href="/dashboard/stocks"
                data-mdb-ripple="true"
                data-mdb-ripple-color="dark"
              >
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  tw="w-3 h-3 mr-3"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 496 512"
                >
                  <path
                    fill="currentColor"
                    d="M336.5 160C322 70.7 287.8 8 248 8s-74 62.7-88.5 152h177zM152 256c0 22.2 1.2 43.5 3.3 64h185.3c2.1-20.5 3.3-41.8 3.3-64s-1.2-43.5-3.3-64H155.3c-2.1 20.5-3.3 41.8-3.3 64zm324.7-96c-28.6-67.9-86.5-120.4-158-141.6 24.4 33.8 41.2 84.7 50 141.6h108zM177.2 18.4C105.8 39.6 47.8 92.1 19.3 160h108c8.7-56.9 25.5-107.8 49.9-141.6zM487.4 192H372.7c2.1 21 3.3 42.5 3.3 64s-1.2 43-3.3 64h114.6c5.5-20.5 8.6-41.8 8.6-64s-3.1-43.5-8.5-64zM120 256c0-21.5 1.2-43 3.3-64H8.6C3.2 212.5 0 233.8 0 256s3.2 43.5 8.6 64h114.6c-2-21-3.2-42.5-3.2-64zm39.5 96c14.5 89.3 48.7 152 88.5 152s74-62.7 88.5-152h-177zm159.3 141.6c71.4-21.2 129.4-73.7 158-141.6h-108c-8.8 56.9-25.6 107.8-50 141.6zM19.3 352c28.6 67.9 86.5 120.4 158 141.6-24.4-33.8-41.2-84.7-50-141.6h-108z"
                  ></path>
                </svg>
                <span>Products</span>
              </a>
            </li>
            <li tw="relative">
              <a
                tw="flex items-center text-sm py-4 px-6 h-12 overflow-hidden text-white overflow-ellipsis whitespace-nowrap rounded hover:text-gray-900 hover:bg-gray-100 transition duration-300 ease-in-out"
                href="#!"
                data-mdb-ripple="true"
                data-mdb-ripple-color="dark"
              >
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  tw="w-3 h-3 mr-3"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                >
                  <path
                    fill="currentColor"
                    d="M192 208c0-17.67-14.33-32-32-32h-16c-35.35 0-64 28.65-64 64v48c0 35.35 28.65 64 64 64h16c17.67 0 32-14.33 32-32V208zm176 144c35.35 0 64-28.65 64-64v-48c0-35.35-28.65-64-64-64h-16c-17.67 0-32 14.33-32 32v112c0 17.67 14.33 32 32 32h16zM256 0C113.18 0 4.58 118.83 0 256v16c0 8.84 7.16 16 16 16h16c8.84 0 16-7.16 16-16v-16c0-114.69 93.31-208 208-208s208 93.31 208 208h-.12c.08 2.43.12 165.72.12 165.72 0 23.35-18.93 42.28-42.28 42.28H320c0-26.51-21.49-48-48-48h-32c-26.51 0-48 21.49-48 48s21.49 48 48 48h181.72c49.86 0 90.28-40.42 90.28-90.28V256C507.42 118.83 398.82 0 256 0z"
                  ></path>
                </svg>
                <span>Reports</span>
              </a>
            </li>
            <li tw="relative">
              <a
                tw="flex items-center text-sm py-4 px-6 h-12 overflow-hidden text-white overflow-ellipsis whitespace-nowrap rounded hover:text-gray-900 hover:bg-gray-100 transition duration-300 ease-in-out"
                href="#!"
                data-mdb-ripple="true"
                data-mdb-ripple-color="dark"
              >
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  tw="w-3 h-3 mr-3"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                ></svg>
                <span onClick={() => logOut()}>Logout</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
