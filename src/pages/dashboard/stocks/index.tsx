import { useAlert } from 'react-alert';
import { useEffect, useState } from 'react';
import axios from '@/app/utils/api';
import Head from 'next/head';
import Masthead from '@/presentation/components/masthead';
import tw, { styled, css } from 'twin.macro';
import { Menu, Transition } from '@headlessui/react';
import { useAppContext } from '@/app/context/AppContext';
import Router, { useRouter } from 'next/router';
import { AiFillEdit, AiTwotoneDelete } from 'react-icons/ai';
import Tabs from 'src/components/stockstabs';
import { useForm } from 'react-hook-form';
import EditProductModal from 'src/components/editproductmodal';

export default function Products() {
  const alert = useAlert();
  const [product, setProduct] = useState([]);

  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    const req = async () => {
      const prod = await axios.get('/product/all_products').then((res) => res.data.data);
      setProduct(prod);
    };
    req();
  }, []);

  const onSubmit = (values) => {
    const obj = JSON.parse(JSON.stringify(values));
    console.log(obj);
    axios
      .post('/product/add_product', {
        barcode: obj.barcode,
        prod_name: obj.pname,
        prod_desc: obj.pdesc,
        prod_price: obj.pprice,
        prod_stocks: obj.pstocks,
      })
      .then((res) => {
        alert.success(res.data.data);
        setShowModal(false);
      });
  };
  return (
    <div>
      <Head>
        <title>Point of Sales - Products</title>
        <meta name="description" content="POS" />
        <link rel="icon" href="/favicon.ico" />

        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet"
        />
      </Head>
      <Masthead />
      <div tw=" md:(relative h-screen flex bg-white)">
        <div tw="md:(ml-40  w-screen right-0 p-2)">
          <div tw="min-w-max flex flex-col space-y-5  ">
            <Tabs />
          </div>
          <div tw="flex">
            <div tw="px-5 height[80vh] w-full overflow-auto">
              <table tw=" text-center w-full h-80 border mt-5 ">
                <thead tw=" bg-white">
                  <tr>
                    <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-2">
                      Actions
                    </th>
                    <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-2">
                      Barcode
                    </th>
                    <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-2">
                      Product
                    </th>
                    <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-2">
                      Price
                    </th>
                    <th scope="col" tw="text-sm font-medium text-gray-900 px-6 py-2">
                      Stocks
                    </th>
                  </tr>
                </thead>
                <tbody id="cursorPointer">
                  {product.map((data, index) => (
                    <tr key={data.id} tw="bg-white border-b">
                      <td tw=" px-6 py-2 whitespace-nowrap text-sm font-medium text-gray-900">
                        <div tw="flex space-x-3 justify-center">
                          <EditProductModal bcode={data.barcode} />
                          <AiTwotoneDelete tw="h-5 w-5 text-red-400 " />
                        </div>
                      </td>
                      <td tw="text-sm text-gray-900 font-light px-6 py-2 whitespace-nowrap">{data.barcode}</td>
                      <td tw="text-sm text-gray-900 font-light px-6 py-2 whitespace-nowrap">
                        {data.prod_name}/{data.prod_desc}
                      </td>
                      <td tw="text-sm text-gray-900 font-light px-6 py-2 whitespace-nowrap">{data.prod_price}</td>
                      <td tw="text-sm text-gray-900 font-light px-6 py-2 whitespace-nowrap">{data.prod_stocks}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
        {/* BIG SCREEN*/}
        <div tw=" hidden md:flex flex-col w-40 h-full shadow bg-white px-1 absolute">
          <ul tw="relative">
            <li tw="relative">
              <a
                tw="flex items-center text-sm py-4 px-6 h-12 overflow-hidden text-black overflow-ellipsis whitespace-nowrap rounded hover:text-gray-900 hover:bg-gray-100 transition duration-300 ease-in-out"
                href="/dashboard"
                data-mdb-ripple="true"
                data-mdb-ripple-color="dark"
              >
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  tw="w-3 h-3 mr-3"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                >
                  <path
                    fill="currentColor"
                    d="M216 0h80c13.3 0 24 10.7 24 24v168h87.7c17.8 0 26.7 21.5 14.1 34.1L269.7 378.3c-7.5 7.5-19.8 7.5-27.3 0L90.1 226.1c-12.6-12.6-3.7-34.1 14.1-34.1H192V24c0-13.3 10.7-24 24-24zm296 376v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h146.7l49 49c20.1 20.1 52.5 20.1 72.6 0l49-49H488c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"
                  ></path>
                </svg>
                <span>Cashier</span>
              </a>
            </li>
            <li tw="relative bg-gray-200">
              <a
                tw="flex items-center text-sm py-4 px-6 h-12 overflow-hidden text-black overflow-ellipsis whitespace-nowrap rounded hover:text-gray-900 hover:bg-gray-100 transition duration-300 ease-in-out"
                href="/dashboard/stocks"
                data-mdb-ripple="true"
                data-mdb-ripple-color="dark"
              >
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  tw="w-3 h-3 mr-3"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 496 512"
                >
                  <path
                    fill="currentColor"
                    d="M336.5 160C322 70.7 287.8 8 248 8s-74 62.7-88.5 152h177zM152 256c0 22.2 1.2 43.5 3.3 64h185.3c2.1-20.5 3.3-41.8 3.3-64s-1.2-43.5-3.3-64H155.3c-2.1 20.5-3.3 41.8-3.3 64zm324.7-96c-28.6-67.9-86.5-120.4-158-141.6 24.4 33.8 41.2 84.7 50 141.6h108zM177.2 18.4C105.8 39.6 47.8 92.1 19.3 160h108c8.7-56.9 25.5-107.8 49.9-141.6zM487.4 192H372.7c2.1 21 3.3 42.5 3.3 64s-1.2 43-3.3 64h114.6c5.5-20.5 8.6-41.8 8.6-64s-3.1-43.5-8.5-64zM120 256c0-21.5 1.2-43 3.3-64H8.6C3.2 212.5 0 233.8 0 256s3.2 43.5 8.6 64h114.6c-2-21-3.2-42.5-3.2-64zm39.5 96c14.5 89.3 48.7 152 88.5 152s74-62.7 88.5-152h-177zm159.3 141.6c71.4-21.2 129.4-73.7 158-141.6h-108c-8.8 56.9-25.6 107.8-50 141.6zM19.3 352c28.6 67.9 86.5 120.4 158 141.6-24.4-33.8-41.2-84.7-50-141.6h-108z"
                  ></path>
                </svg>
                <span>Products</span>
              </a>
            </li>
            <li tw="relative">
              <a
                tw="flex items-center text-sm py-4 px-6 h-12 overflow-hidden text-black overflow-ellipsis whitespace-nowrap rounded hover:text-gray-900 hover:bg-gray-100 transition duration-300 ease-in-out"
                href="#!"
                data-mdb-ripple="true"
                data-mdb-ripple-color="dark"
              >
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  tw="w-3 h-3 mr-3"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                >
                  <path
                    fill="currentColor"
                    d="M192 208c0-17.67-14.33-32-32-32h-16c-35.35 0-64 28.65-64 64v48c0 35.35 28.65 64 64 64h16c17.67 0 32-14.33 32-32V208zm176 144c35.35 0 64-28.65 64-64v-48c0-35.35-28.65-64-64-64h-16c-17.67 0-32 14.33-32 32v112c0 17.67 14.33 32 32 32h16zM256 0C113.18 0 4.58 118.83 0 256v16c0 8.84 7.16 16 16 16h16c8.84 0 16-7.16 16-16v-16c0-114.69 93.31-208 208-208s208 93.31 208 208h-.12c.08 2.43.12 165.72.12 165.72 0 23.35-18.93 42.28-42.28 42.28H320c0-26.51-21.49-48-48-48h-32c-26.51 0-48 21.49-48 48s21.49 48 48 48h181.72c49.86 0 90.28-40.42 90.28-90.28V256C507.42 118.83 398.82 0 256 0z"
                  ></path>
                </svg>
                <span>Reports</span>
              </a>
            </li>
            <li tw="relative">
              <a
                tw="flex items-center text-sm py-4 px-6 h-12 overflow-hidden text-black overflow-ellipsis whitespace-nowrap rounded hover:text-gray-900 hover:bg-gray-100 transition duration-300 ease-in-out"
                href="#!"
                data-mdb-ripple="true"
                data-mdb-ripple-color="dark"
              >
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  tw="w-3 h-3 mr-3"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 512 512"
                ></svg>
                <span>Logout</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}

const editBtn = { color: 'gray', fontSize: '1em' };

const Spanner = tw.span`
text-gray-600
font-bold
font-family[Arial]
font-size[13px]
bg-blend-color
`;

const Spanner2 = tw.span`
text-gray-600

font-bold
font-family[-apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif]
font-size[13px]
`;

const Tdata = tw.td`
border-b
border-l
p-2
text-center

`;

const Thead = tw.th`

p-2
bg-gradient-to-b
from-gray-200
to-gray-100
border-b
text-center

`;
const Trow = tw.tr`
hover:bg-amber-100
bg-white


`;

const ListBtn = tw.button`
text-left bg-white py-1 px-4 text-black
hover:(bg-sky-700) 
active:(scale-95)
`;

const NavBtn = tw.button`
text-white
mr-6
hover:(text-amber-600 scale-95)

`;
const styles = {
  // Move long tw sets out of jsx to keep it scannable
  container: ({ hasBackground }) => [hasBackground && tw`bg-gradient-to-b from-white to-white`],
};

const ContentTransition = styled(Transition)`
  &.enter {
    ${tw`ease-out duration-100`}
  }
  &.enterFrom {
    ${tw`opacity-0`}
  }
  &.enterTo {
    ${tw`opacity-100 z-[1]`}
  }
  &.leave {
    ${tw`ease-in duration-100`}
  }
  &.leaveFrom {
    ${tw`opacity-100`}
  }
  &.leaveTo {
    ${tw`opacity-0`}
  }
`;
