import { useAlert } from 'react-alert';
import { useEffect, useState } from 'react';
import axios from 'axios';
import Head from 'next/head';
import Link from 'next/link';
import Masthead from '@/presentation/components/masthead';
import tw from 'twin.macro';

import { MAIN_URL } from '@/presentation/components/masthead';

import { useAppContext } from '@/app/context/AppContext';

export default function Cashierinfo() {
  const alert = useAlert();
  const [cashier, setCashier] = useState([]);
  const { addCashierDialog, setAddCashierDialog } = useAppContext();

  useEffect(() => {
    const req = async () => {
      const cash = await axios
        .get(`${MAIN_URL}view_all_cashier`, {
          headers: {
            Authorization: 'Bearer ' + localStorage.getItem('token'),
          },
        })
        .then((res) => res.data.cashierinfo);
      setCashier(cash);
    };
    req();
  }, []);

  return (
    <div>
      <Head>
        <title>Point of Sales - Cashier</title>
        <meta name="description" content="POS" />
        <link rel="icon" href="/favicon.ico" />

        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet"
        />
      </Head>
      <Masthead />

      <div tw="flex flex-col  bg-gray-600 height[100vh] w-full p-4">
        <p tw="text-white">Cashier Information</p>

        <div tw="bg-gray-100 rounded-md flex flex-col p-2 border-2 height[60vh]">
          <div tw="flex border-gray-600 p-1 rounded-md  "></div>
          <div>Cashier ID: 1321515</div>
          <div>Cashier Name: Nadzkie</div>
        </div>
      </div>
    </div>
  );
}

const editBtn = { color: 'black', fontSize: '1em' };

const Spanner = tw.span`
text-white
font-bold
font-family[Arial]
font-size[13px]
bg-blend-color
`;

const Spanner2 = tw.span`
text-black
font-bold
font-family[Arial]
font-size[13px]
`;

const Tdata = tw.td`
border-2
border-gray-600
text-center
bg-gray-200
`;

const Thead = tw.th`
border-2
border-gray-600
p-2
bg-sky-700
text-center
`;
const Trow = tw.tr`

`;

const ListBtn = tw.button`
text-left bg-white py-1 px-4 text-black
hover:(bg-sky-700) 
active:(scale-95)
`;

const NavBtn = tw.button`
text-white
mr-6
hover:(text-amber-600 scale-95)

`;
