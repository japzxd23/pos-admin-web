import { useAlert } from 'react-alert';
import { useEffect, useState } from 'react';
import axios from '@/app/utils/api';
import Head from 'next/head';
import Masthead from '@/presentation/components/masthead';
import tw, { styled } from 'twin.macro';
import Modal from 'react-modal';
import { BsFillPersonPlusFill } from 'react-icons/bs';
import { GrFormNext } from 'react-icons/gr';
import Addcashier from '@/presentation/components/addcashier';
import { useAppContext } from '@/app/context/AppContext';
import { MAIN_URL } from '@/presentation/components/masthead';
import { useRouter } from 'next/router';
import { MdExpandMore } from 'react-icons/md';
import { Menu, Transition } from '@headlessui/react';
import Link from 'next/link';
export default function Cashier() {
  const alert = useAlert();
  const [cashier, setCashier] = useState([]);
  const { addCashierDialog, setAddCashierDialog } = useAppContext();
  const { actionCode, setActionCode } = useAppContext();
  const URL = `${MAIN_URL}`;
  const router = useRouter();

  useEffect(() => {
    (async () => {
      const cash = await axios.get('/view_all_cashier').then((res) => res.data.data);
      setCashier(cash);
    })();
  }, []);

  return (
    <div>
      <Head>
        <title>Point of Sales - Cashier</title>
        <meta name="description" content="POS" />
        <link rel="icon" href="/favicon.ico" />

        <link
          href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet"
        />
      </Head>
      <Masthead />

      <div css={styles.container({ hasBackground: true })} tw="flex flex-col height[100vh] w-full p-4">
        <div tw="flex space-x-1 items-center">
          <Link href="/dashboard">Home</Link>
          <GrFormNext />
          <p tw="text-gray-600">Cashier Information</p>
        </div>
        <div tw="bg-white   shadow-md rounded-md mb-1 p-1">
          <div tw="flex">
            <button tw="flex  text-center p-2 rounded text-white hover:bg-amber-400 focus:outline-none my-1" onClick={() => setAddCashierDialog(true)}>
              <p tw="flex p-1 text-black">
                <BsFillPersonPlusFill tw="mr-3 font-size[1.5em]" />
                Add Cashier
              </p>
              <Modal isOpen={addCashierDialog} ariaHideApp={false} tw="flex p-3 w-full  height[100vh] justify-center items-center shadow-2xl">
                <Addcashier />
              </Modal>
            </button>
          </div>
        </div>
        <div tw="bg-gray-100 overflow-x-scroll shadow-md rounded-md flex flex-col border-2 height[60vh]">
          <table tw=" w-full">
            <thead>
              <Trow>
                <Thead>
                  <Spanner>Cashier</Spanner>
                </Thead>
                <Thead>
                  <Spanner>Username</Spanner>
                </Thead>
                <Thead>
                  <Spanner>Email</Spanner>
                </Thead>
                <Thead>
                  <Spanner>Actions</Spanner>
                </Thead>
              </Trow>
            </thead>

            <tbody>
              {cashier.map((data, index) => (
                <Trow key={data.cashier_id}>
                  <Tdata>
                    <Spanner2>{data.cashier}</Spanner2>
                  </Tdata>

                  <Tdata>
                    <Spanner2>{data.username}</Spanner2>
                  </Tdata>
                  <Tdata>
                    <Spanner2>{data.email}</Spanner2>
                  </Tdata>
                  <Tdata>
                    <div tw="flex justify-center">
                      <div tw="relative">
                        <Menu>
                          <Menu.Button tw="px-3 py-1 border  hover:(bg-amber-100) border-gray-300">
                            <div tw="flex items-center space-x-2">
                              <span>...</span>
                              <MdExpandMore tw="h-4 w-4" />
                            </div>
                          </Menu.Button>

                          <ContentTransition enter="enter" enterFrom="enterFrom" enterTo="enterTo" leave="leave" leaveFrom="leaveFrom" leaveTo="leaveTo">
                            <Menu.Items tw="absolute right-[4rem] z-[1] py-2 flex flex-col bg-white rounded-lg shadow-md max-w-max">
                              <Menu.Item>
                                {({ active }) => (
                                  <button
                                    onClick={() => {
                                      console.log(cashierinfo.cashier);
                                    }}
                                    tw="flex-shrink-0 w-[12rem] py-3 px-4 text-left transition duration-100 ease-in-out"
                                    css={[active && tw`bg-gray-200`]}
                                  >
                                    Account settings
                                  </button>
                                )}
                              </Menu.Item>
                              <Menu.Item>
                                {({ active }) => (
                                  <button tw="flex-shrink-0 w-[12rem] py-3 px-4 text-left transition duration-100 ease-in-out" css={active && tw`bg-gray-200`}>
                                    View Sales
                                  </button>
                                )}
                              </Menu.Item>
                              <Menu.Item>
                                {({ active }) => (
                                  <button tw="flex-shrink-0 w-[12rem] py-3 px-4 text-left transition duration-100 ease-in-out" css={active && tw`bg-red-200`}>
                                    Remove
                                  </button>
                                )}
                              </Menu.Item>
                            </Menu.Items>
                          </ContentTransition>
                        </Menu>
                      </div>
                    </div>
                  </Tdata>
                </Trow>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

const editBtn = { color: 'gray', fontSize: '1em' };

const Spanner = tw.span`
text-white
font-bold
font-family[Arial]
font-size[13px]
bg-blend-color
`;

const Spanner2 = tw.span`
text-black
font-bold
font-family[Arial]
font-size[13px]
`;

const Tdata = tw.td`
border-b
border-l


text-center
height[10vh]
`;

const Thead = tw.th`

p-2
bg-sky-700
text-center
`;
const Trow = tw.tr`

`;

const ListBtn = tw.button`
text-left bg-white py-1 px-4 text-black
hover:(bg-sky-700) 
active:(scale-95)
`;

const NavBtn = tw.button`
text-white
mr-6
hover:(text-amber-600 scale-95)

`;
const styles = {
  // Move long class sets out of jsx to keep it scannable
  container: ({ hasBackground }) => [hasBackground && tw`bg-gradient-to-b from-white to-white`],
};

const ContentTransition = styled(Transition)`
  &.enter {
    ${tw`ease-out duration-100`}
  }
  &.enterFrom {
    ${tw`opacity-0`}
  }
  &.enterTo {
    ${tw`opacity-100 z-[1]`}
  }
  &.leave {
    ${tw`ease-in duration-100`}
  }
  &.leaveFrom {
    ${tw`opacity-100`}
  }
  &.leaveTo {
    ${tw`opacity-0`}
  }
`;
