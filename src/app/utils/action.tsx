import axios from '@/app/utils/api';

export const removeProduct = (barcode) => {
  axios.get('/product/remove/' + barcode);
};
