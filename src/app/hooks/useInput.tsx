import React, { useState } from 'react';

export function useInput(defaultValue = '') {
  const [value, setValue] = useState(defaultValue);

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const onClear = () => {
    setValue('');
  };

  return {
    value,
    onChange: handleChange,
    clear: onClear,
  };
}
