import { route } from 'next/dist/server/router';
import { useRouter } from 'next/router';
import { createContext, useContext, useEffect, useState } from 'react';

export const AppContext = createContext<any>({});
export const useAppContext = () => useContext(AppContext);

const protectedRoute = ['/dashboard', '/cashiers', '/stocks'];

export default function AppContextProvider({ children }) {
  const [addCashierDialog, setAddCashierDialog] = useState(false);
  const [addProductDialog, setAddProductDialog] = useState(false);
  const [editProdDialog, setEditProdDialog] = useState(false);
  const [actionCode, setActionCode] = useState('');
  const [showModal, setShowModal] = useState(false);
  const router = useRouter();

  useEffect(() => {
    const token = localStorage.getItem('token');
    if (protectedRoute.includes(router.pathname) && !token.includes(token)) {
      router.replace('/');
    }
  }, [router]);

  return (
    <AppContext.Provider
      value={{
        addCashierDialog,
        setAddCashierDialog,
        addProductDialog,
        setAddProductDialog,
        actionCode,
        setActionCode,
        editProdDialog,
        setEditProdDialog,
        showModal,
        setShowModal,
      }}
    >
      {children}
    </AppContext.Provider>
  );
}
